console.log("ahoj")

let express = require('express');
let app = express();

app.get('/', function (req, res) { // get(je to metoda) ma dva parametre, ten prvy je URLka; ak pride mojmu pcitacu poziadavka (URL) tak sprav toto
    res.send("<h1> hello </h1>" + new Date() + JSON.stringify(req.query));  // req (objekt, kt. reprezentuje data, ktore vyslal klientsky prehliadac) teraz nepouzivame a res(response) hovori, ze poslem klientovi tentokrat obyc string
   
});


// app.get('/a', function (req, res) {
//     res.send("<h1> hello </h1>" + req.query.name);  // do prehliadaca http://localhost:3000/a?name=jozko
   
// });

// app.get('/b', function (req, res) {
//     res.send(`Hello ${req.query.name}`);  // do prehliadaca http://localhost:3000/b?name=jozef
   
// });

// app.get('/a/*', function (req, res) {
//     res.send(`Hello ${JSON.stringify(req.params)}`);
   
// });

app.get('/a/:x/:y', function (req, res) {
    res.send(`Hello ${JSON.stringify(req.params)}`); //takto vyzera Url http://localhost:3000/a/nieco/volaco
   
});
app.get('/aaa', function(req, res){
   res.send("waaaaaa");
});

app.listen(3000); // app pocuvaj na porte 3000

